console.log("Hello Alpha")

//JSON Javascript Object Notation
/*
    {
        "propeties" : "value"
    } 
 */

    let sample1 = `
        {
            "name" : "Cardo Dalisay",
            "age" : 20,
            "address" : {
                "city" : "Quezon City",
                "country" : "Philippines"
            }
        }
    `;
    console.log(sample1);
    
    let person = `{

        "name" : "Alpha Joy",
        "age" : 27,
        "address" : {
            "city" : "Tabuk City",
            "country" : "Philippines"
        }
    }`;
    console.log(typeof person)

//JSON.parse()

console.log(JSON.parse(sample1));

//JSON Array

let sampleArr = `
    [
        {
            "email" : "sinayangmoko@gmail.com",
            "password" : "feb14",
            "isAdmin" : false
        },
        {
            "email" : "dalisay@cardo.com",
            "password" : "thecountryman",
            "isAdmin" : false
        },
        {
            "email" : "jsonv@gmail.com",
            "password" : "friday13",
            "isAdmin" : false
        }
    ]
`;
console.log(sampleArr);
console.log(typeof sampleArr);

let parseSampleArry = JSON.parse(sampleArr);

console.log(parseSampleArry);
console.log(typeof parseSampleArry);

console.log(parseSampleArry.pop());
console.log(parseSampleArry);

//JSON.stringify

sampleArr = JSON.stringify(parseSampleArry);
console.log(sampleArr);

//Database (JSON)

let jsonArry =`
    [
        "Pizza",
        "hamburger",
        "shaghai",
        "hotdof stick an a pineapple",
        "pancit bihon"
    ]
`;
console.log(jsonArry);

let foodPanda = JSON.parse(jsonArry);
console.log(foodPanda[4]="Fries");

jsonArry = JSON.stringify(foodPanda);
console.log(jsonArry);

let firstName = prompt("what is you name");
let lastName = prompt("what is you lastName");
let age = prompt("How old are you?");
let address = {
    city: prompt("city"),
    country: prompt("country"),
}

let otherData = {
    
}